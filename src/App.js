import React, {Component} from 'react';
import './App.css';
import AddTaskForm from './AddTaskForm/AddTaskForm';
import Task from './Task/Task';

class App extends Component {
    state = {
        tasks: [
            {text: 'Do homework', id: 213413245},
            {text: 'Buy milk', id: 21341234},
            {text: 'Go to cinema', id: 12351325}
        ],
        currentTask: ''
    };

    addNewTaskText = (event) => {
        this.setState({currentTask: event.target.value});
    };

    addNewTask = e => {
        e.preventDefault();

        if (this.state.currentTask !== '') {
            const currentTask = this.state.currentTask;
            const date = new Date();
            const time = date.getTime();
            const newTask = {
                text: currentTask,
                id: time
            };

            const tasks = [...this.state.tasks];
            tasks.unshift(newTask);

            this.setState({tasks, currentTask: ''});
        } else {
            alert('Please add task text!');
        }

    };

    removeTask = (id) => {
        const index = this.state.tasks.findIndex(t => t.id === id);
        const tasks = [...this.state.tasks];
        tasks.splice(index, 1);

        this.setState({tasks});
    };


    render() {

        const tasks = (
            <div className="tasks">
                {this.state.tasks.map((task) => {
                    return <Task
                        key={task.id}
                        task={task.text}
                        remove={() => this.removeTask(task.id)}
                    />
                })}
            </div>
        );

        return (
            <div className="App">
                <AddTaskForm
                    newTaskText={this.addNewTaskText}
                    newTask={this.addNewTask}
                    text={this.state.currentTask}
                />
                {tasks}
            </div>
        );
    }
}

export default App;
