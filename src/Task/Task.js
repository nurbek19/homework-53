import React from 'react';

const Task = props => {
    return (
        <div className="task">
            <p>{props.task}</p>
            <span onClick={props.remove}>delete</span>
        </div>
    )
};

export default Task;
